# event description text
def introduction():
    print('Welcome to my game. When you are asked a question the words in capitals are the answers. I hope you like my game.\n')
    print('~-----------------~')
    print('{ T H E M A G I C }')
    print('(∩ ` -´)⊃━━☆ﾟ.*･｡ﾟ')
    print('~-----------------~')
    

def old_castle():
    print ('\nYou have been invited to meet at The Old Castle to learn magic. When you arrive you are greeted by your guide Wana.')
    print ('Wana - I will be your guide if you take this quest you shall learn magic, defeat evil and meet The Magic Racoon (an ancient master).')

def forest_entrance():
    print('Wana - That is the best idea to save your people, you shall start your quest right away at The Forest of The Moon.')
     

def wise_fox():
    print('You meet a wise fox on the outskirts of the forest.')

def hedgehog():
    print('\nWise Fox - well done you may enter The Forest of The Moon.')
    print('You enter the forest and see a hedgehog that seems to be enchanted by The Dark.')
    print('You also see an old worn out book you open it and learn the spell for unenchanting.')
    print(' -------------------------------------------------------------------------')
    print('| The Spell of Unenchant            |                                     |')
    print('| You need to focus your mind on the| any other modern language. Lots of  |')
    print('| object that you want to unenchant | the elder mages don\'t aprove of     |')
    print('| then imagine saying ανούσια or    | them but personally I think it is   |')
    print('| anoúsia(Greek word for unenchant).| good that we are still creating more|')
    print('| This is because in the days of old| spells.                             |')
    print('| when magic was created they spoke | You see the reason the elder mages  |')
    print('| Greek and Latin so most spells    | do not agree with it is because they|')
    print('| are now in Greek or Latin.        | went through the war and a historic |')
    print('| However some spells that are known| leader unofficially said that all   |')
    print('| as modern spells are in English,  | should be in only Greek and Latin   |')
    print('| German, Spanish, Japenese or      | but another book will explain that. |')
    print(' -------------------------------------------------------------------------')


def companion():
    print('The hedgehog is very thankful and vows to be loyal to you as your compainion.\n')
    print('Suddenly out of nowhere Wana comes towards you.\n')
    print('Wana - I forgot to tell you The Forest of The Moon is called this because when the moon is out all the creatures and plants change. Nobody is sure how they change or what they look like when they change because no one has come out of the forest that has been there at night to tell us. So I suggest that you don\'t stay here at night and quickly get to the other side of the forest before it is too late.')

def light():
    print('\nHearing Wana\'s warning you carry on as quickly as possible.')
    print('\nIn the distance you see a light but you also see a dirt path.')

def hills():
    print('You go towards the light and come out of the forest.')
    print('Then Wana comes towards you.')
    print('Wana - I see you have made your way out of The Forest of The Moon, well done. What you now see are the Nebulums (storm makers), if you want to stop and rest when travelling through these hills it must be in a cave or you will be attacked by te storms they create. Check if there is anything in the cave before you set up camp.')

def rest():
    print('You are getting really tired (-_-)zZZ, you look up and see that it is getting quite dark now and you can see that the dirt path goes for quite a bit longer.')


def village():
    print('You go travel back home but your village has been destroyed because of The Dark.')

# game over texts
def gameover_forest():
    print('You go back to your family who are already dead, you decide that you have nothing to live for so you commit suicide...')
    print('GAME OVER!')

def gameover_wise_fox():
    print('Wise Fox- you may not enter for only noble, wise people should enter this forest.')
    
def gameover_hedgehog():
    print('You walk off but then suddenly you see the hedgehog infront of you with red eyes and within a blink of the eye it eats you...')
    print('GAME OVER!')

def gameover_rest():
    print('You sit down underneath a tree to rest and before you know it you have fallen asleep and in your dream you imagine Wana - I told you not to stay in the forest at night. Then you realise that you have made a terrible mistake so you wake up but it is too late all the animals and planets have already changed.')
    print('GAME OVER!')

def gameover_village():
    print('You rush over to your family and die with them...')
    print('GAME OVER!')