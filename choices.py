def lower_case_choice(text, valid_choices):
	user_choice = None
	while user_choice not in valid_choices:
		if user_choice:
			print('Valid options are ' + ','.join(valid_choices))
		user_choice = input(text + '\n').lower()
	return user_choice

def old_castle():
	return lower_case_choice('Do you say YES or NO?', ['yes', 'no'])
	

def forest_entrance():
	return lower_case_choice('Do you go on and enter The Forest of The Moon YES or NO?', ['yes', 'no'])

def wise_fox():
	return lower_case_choice("""Wise Fox - what can you see, but not feel, hear or taste and can only be found at night
			1. A SHADOW\n
			2. LIGHT\n
			3. THE DARK\n
			""", ['a shadow', 'light', 'the dark'])

def hedgehog():
	return lower_case_choice('Do you UNENCHANT the hedgehog or LEAVE it?', ['unenchant', 'leave'])

def light():
	return lower_case_choice('Do you go towards the LIGHT or follow the PATH?', ['light', 'path'])

def hills():
	return lower_case_choice('Do you start TRAVELLING through the Nebulums or STOP?', ['travelling', 'stop'])
	
def rest():
	return lower_case_choice('Do you stop and REST or CARRY ON?', ['rest', 'carry on'])

def village():
	return lower_case_choice('Do you DIE with your family or try and SAVE your village?', ['die', 'save'])


