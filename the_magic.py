import events
import text
import choices

def old_castle():
    old_castle_choice = events.old_castle()
    if old_castle_choice == 'yes':
        forest()
    else:
        village()

def village():
    village_choice = events.village()
    if village_choice == 'save':
        print('You go to find your guide Wana.')
        print('Wana - I knew that you would come back in the end.')
        forest()
    elif village_choice == 'die':
        text.gameover_village()

def forest():
    forest_entrance_choice = events.forest_entrance()
    if forest_entrance_choice == 'yes':
        wise_fox()
    elif forest_entrance_choice == 'no':
        text.gameover_forest()

def wise_fox():
    wise_fox_choice = events.wise_fox()
    if 'dark' in wise_fox_choice:
        hedgehog()
    else:
        text.gameover_wise_fox()
        text.gameover_forest()

def hedgehog():
    hedgehog_choice = events.hedgehog()
    if hedgehog_choice == 'unenchant':
        text.companion()
        light()
    elif hedgehog_choice == 'leave':
        text.gameover_hedgehog()

def light():
    light_choice = events.light()
    if light_choice == 'light':
        hills()
    elif light_choice == 'path':
        rest()

def hills():
    hills_choice = events.hills()
    if hills_choice == 'travelling':
        print('TO BE CONTINUED...')
    elif hills_choice == 'stop':
        text.gameover_forest()

def rest():
    rest_choice = events.rest()
    if rest_choice == 'carry on':
        hills()
    elif rest_choice == 'rest':
        text.gameover_rest()
     



# The adventure starts at the old castle, so call old castle function
text.introduction()
old_castle()

            



	

                
