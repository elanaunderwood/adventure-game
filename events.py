import text
import choices

def introduction():
    text.introduction()

def old_castle():
    text.old_castle()
    return choices.old_castle()

def forest_entrance():
    text.forest_entrance()
    return choices.forest_entrance()

def wise_fox():
    text.wise_fox()
    return choices.wise_fox()

def hedgehog():
    text.hedgehog()
    return choices.hedgehog()

def light():
    text.light()
    return choices.light()

def hills():
    text.hills()
    return choices.hills()

def rest():
    text.rest()
    return choices.rest()
    

def village():
    text.village()
    return choices.village()
    